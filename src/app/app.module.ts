//import { MessageComponent } from './messages/message/message.component';
import { UsersComponent } from './users/users.component';
import {RouterModule} from '@angular/router';
import{HttpModule} from '@angular/http'
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessegesFormComponent } from './messages/messeges-form/messeges-form.component';
import {FormsModule,  ReactiveFormsModule } from "@angular/forms";
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessegesFormComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'', component:MessagesComponent},
      {path:'users', component:UsersComponent},
      {path:'message/:id', component:MessageComponent},
      {path:'login',component:LoginComponent},
      {path:'**',component:NotFoundComponent}
    ])
  ],
  providers: [
    MessagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
