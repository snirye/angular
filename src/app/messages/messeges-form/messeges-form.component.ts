import { MessagesService } from './../messages.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'messegesForm',
  templateUrl: './messeges-form.component.html',
  styleUrls: ['./messeges-form.component.css']
})
export class MessegesFormComponent implements OnInit {

  @Output() addMessage: EventEmitter<any> = new EventEmitter<any>();  //יצירת אירוע- המידע יכול להיות כל דבר. לא מוגבל לסוג מידע מסויים
  @Output() addMessagePs: EventEmitter<any> = new EventEmitter<any>(); 

  service:MessagesService;
  msgform = new FormGroup({     
    message:new FormControl(),
    user:new FormControl(),

  });

  sendData(){
    this.addMessage.emit(this.msgform.value.message);   //בזמן שליחה- זה המידע שיועבר
    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
      response=>{
        console.log(response.json());
        this.addMessagePs.emit();

      }
    )

  };

  constructor(service:MessagesService) { 
    this.service = service;

}

  ngOnInit() {
  }

}