import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessegesFormComponent } from './messeges-form.component';

describe('MessegesFormComponent', () => {
  let component: MessegesFormComponent;
  let fixture: ComponentFixture<MessegesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessegesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessegesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
