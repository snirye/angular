import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import 'rxjs/Rx';


@Injectable()
export class MessagesService {
  http:Http;                      //שמאל- מחלקה, ימין- תכונה
  getMessages(){
    //return ['Message1','Message2','Message3','Message4'];
    //get messages from the SLIM rest API (Don't say DB)
    let token = localStorage.getItem('token');
    let options= {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }

    return this.http.get('http://localhost/angular/slim/messages',options);

  }

  getMessage(id){
  return this.http.get('http://localhost/angular/slim/messages/' + id);

    }


  postMessage(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);

    return this.http.post('http://localhost/angular/slim/messages',
    params.toString(),options);
  }



  login(credentials){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
      }
      let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
      return this.http.post('http://localhost/angular/slim/auth',params.toString(),options).map(response=>{
        let token = response.json().token;
        if(token) localStorage.setItem('token',token);
        console.log(token);

      });
    

  }


  constructor(http:Http) {        //יצירת אינסטנס
    this.http= http;             // השמה של האובייקט
  }

  deleteMessage(key){
    return this.http.delete('http://localhost/angular/slim/messages/' + key);
  }



}
